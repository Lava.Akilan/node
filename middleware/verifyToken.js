const jsonwebtoken = require('jsonwebtoken');

async function checkTokenExist(req, res) {
    const headertoken = req.headers.authorization;
    if(headertoken === undefined  ){
        return res.status(403).json({
            status:403,
            message: "Access Denied! Unauthorized User"
        });
    } 
    const bearertoken = headertoken.split(" ");
    const token = bearertoken[1];
    jsonwebtoken.verify(token, process.env.SECRET_KEY, (err, authData)=>{
        if(err){
            return res.status(401).json({
                status: 401,
                message: "Invalid Token..."
            });
        } 
        req.params.userid = authData.user.id;
        req.params.role = authData.user.roleType;
        res.cookie('userrole', req.params.role, { httpOnly: true, secure: true, SameSite: 'strict' , expires: new Date(Number(new Date()) + 30*60*1000) }); //we add secure: true, when using https.
    });
}
async function  verifyTokenandAllowadmin  (req, res, next){
        
        await checkTokenExist(req, res);
        console.log(req.cookies.userrole, 'cookie -role')
        if(req.cookies.userrole != 1){
            return res.status(401).json({
                status:401,
                message: "Access Denied! you are not an Admin"
            });
        }  
        if(req.params.userid) {
            next();
        } 
 }
 async function  verifyUserToken  (req, res, next){
    await checkTokenExist(req, res);
    if(req.params.userid) {
        next();
    }
}

module.exports = {
    verifyTokenandAllowadmin,
    verifyUserToken
}