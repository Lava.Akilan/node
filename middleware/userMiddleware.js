const userControl = require('../controllers/user');
const userschema = require('../schemas/userSchemas');
async function checkDuplication (req, res, next) {
    const { error } = userschema.userschemas(req.body); 
    const valid = error == null; 
    if (!valid) { 
        const { details } = error; 
        const message = details.map(i => i.message).join(',');
    
        return res.status(422).json({ status:422, error: message }) ;
    }
    let user = await userControl.getUserByEmail(req.body.email);
    if(user){
        return res.status(422).json({
            status:422,
            message: "Email already exists"
        });
    }
    next();
}
async function checkDuplicationUpdate (req, res, next) {
    const { error } = userschema.userschemasUpdate(req.body); 
    const valid = error == null; 
    if (!valid) {
        const { details } = error; 
        const message = details.map(i => i.message).join(',');
    
        return res.status(422).json({ status:422, error: message }); 
    }
    if(req.body.email) {
        let user = await userControl.getUserByEmail(req.body.email, req.params.id);
        if(user){
            return res.status(422).json({
                status:422,
                message: "Email already exists"
            })
        }
    } 
    next();
}
async function validationUrl (req, res, next) {
    const { error } = userschema.idValidate(req.params.id); 
    const valid = error == null; 
    if (!valid) { 
        const { details } = error; 
        const message = details.map(i => i.message).join(',');
    
        return res.status(422).json({ status:422, error: message }) 
    }
    next();
}

async function fileValidate (req, res, next) {
    const fileData = req.file;
    if(!fileData) {
        return res.status(422).json({ status:422, error: 'Please upload the file' });
    }
    const  error  = userschema.fileValidate(fileData); 
    if (error.value != '' && error.value != null) {         
        return res.status(422).json({ status:422, error: error }) 
    }
    next();
}
async function  checkPassword (req, res, next) {
    const { error } = userschema.passwordMatch(req.body); 
    const valid = error == null; 
    if (!valid) { 
        const { details } = error; 
        const message = details.map(i => i.message).join(',');
    
        return res.status(422).json({ status:422, error: message });
    } 
    next();
}
module.exports = {
    checkDuplication,
    checkDuplicationUpdate,
    validationUrl,
    fileValidate,
    checkPassword
}