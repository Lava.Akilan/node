const authControl = require('../controllers/authController');
const userschema = require('../schemas/userSchemas');
async function registerCheck (req, res, next) {
    const { error } = userschema.userschemas(req.body); 
    const valid = error == null; 
    if (!valid) {
        const { details } = error; 
        const message = details.map(i => i.message).join(',');
    
        return res.status(422).json({ status:422, error: message }) ;
    }
    let user = await authControl.getUserEmail(req.body.email);
    if(!user){
        return res.status(422).json({
            status:422,
            message: "Email already exists"
        })
    }
    next();
}
async function loginVerfiy (req, res, next) {

    let user = await authControl.getUserEmail(req.body.email);
    if(!user){
        return res.status(404).json({
            status:404,
            message: "Email does not exists"
        })
    }
    next();
}

module.exports = {
    registerCheck,
    loginVerfiy
}
    