const emailValidate = require('../schemas/emailSchemas');
async function payLoadValidate (req, res, next) {
    const { error } = emailValidate.emailValidation(req.body); 
    const valid = error == null; 
    if (!valid) {
        const { details } = error; 
        const message = details.map(i => i.message).join(',');
    
        return res.status(422).json({ status:422, error: message });
    }
    next();
}
async function validationString (req, res, next) {
    const { error } = emailValidate.stringValidate(req.body.mailkey); 
    const valid = error == null; 
    if (!valid) { 
        const { details } = error; 
        const message = details.map(i => i.message).join(',');
    
        return res.status(422).json({ status:422, error: message }); 
    }
    next();
}
module.exports = {
    payLoadValidate,
    validationString
}