const createError = require('http-errors');
const bodyParser = require('body-parser');
const path = require('path');
const cookieParser = require('cookie-parser');
const express = require('express');
const logger = require('morgan');
require('dotenv').config()

//route defining
const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const usersSearchRouter = require('./routes/userSearch');
const product = require('./routes/product');
const email = require('./routes/email');

//middleware check to verify the cookie token
const verfiyCookieToken = require('./middleware/verifyToken');

//intial the app
const app = express();
app.disable("x-powered-by");

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/usersearch', verfiyCookieToken.verifyTokenandAllowadmin, usersSearchRouter);
app.use('/product', verfiyCookieToken.verifyTokenandAllowadmin, product);
app.use('/email', verfiyCookieToken.verifyTokenandAllowadmin, email);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
