'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Producttopics extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Producttopics.init({
    topics: DataTypes.INTEGER,
    product: DataTypes.INTEGER,
    brand: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'Producttopics',
    // Specify the table name
    tableName: 'producttopics',
  });
  return Producttopics;
};