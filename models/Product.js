'use strict';
const {
  Model
} = require('sequelize');
const Topic  = require('./Topic');
const ProdTopMap = require('./Producttopics');

module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
        this.belongsToMany(models.Topic, {
            as: 'topics',
            through: 'ProductTopics', foreignKey: 'product', otherKey: 'topics'
        });
    }
  }
  Product.init({
    product_name: DataTypes.STRING,
    product: DataTypes.STRING,
    jobcode: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'Product',
    // Specify the table name
    tableName: 'product',
  });
  return Product;
};