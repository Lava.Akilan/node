'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Topic extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsToMany(models.Product, {
        as: 'products', through: models.Producttopics,
        foreignKey: 'TopicId'
    });
    }
  }
  Topic.init({
    topic_title: DataTypes.STRING,
    presentation_title: DataTypes.STRING,
    topic_description: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'Topic',
    // Specify the table name
    tableName: 'mastertopics',
  });
  return Topic;
};