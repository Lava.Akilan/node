'use strict';
const {
  Model
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Mastermailtemplate extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Mastermailtemplate.init({
    templateName: DataTypes.STRING,
    template: DataTypes.STRING,
    mailKey: DataTypes.STRING,
    subject: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Mastermailtemplate',
    // Specify the table name
    tableName: 'mastermailtemplate',
  });
  return Mastermailtemplate;
};