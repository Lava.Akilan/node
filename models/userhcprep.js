'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserHcpRep extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  UserHcpRep.init({
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    fullName: DataTypes.STRING,
    email: DataTypes.STRING,
    telephone:DataTypes.STRING,
    degree:DataTypes.STRING,
    speciality:DataTypes.STRING,
    address:DataTypes.STRING,
    city:DataTypes.STRING,
    state:DataTypes.STRING,
    photo:DataTypes.STRING,
    photo_path:DataTypes.STRING,
    npi:DataTypes.STRING,
    address2:DataTypes.STRING,
    state_license_no:DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'UserHcpRep',
    // Specify the table name
    tableName: 'userhcprep',
  });
  return UserHcpRep;
};