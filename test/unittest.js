
const chai =  require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');
const apiRequest = require('./testReponse');
chai.use(chaiHttp);
chai.should();

describe('Api Testing', () => {
let token = '';
let registerPayload = {
    "email": process.env.SAMPLE_EMAIL,
    "name":"Name unit test",
    "password": process.env.SAMPLE_PASS
};
let wrongRegisterPayload = {
    "email": process.env.SAMPLE_EMAIL,
    "name":"Name unit test",
    "password": ""
};
let updatePayload = {
    "email": process.env.SAMPLE_EMAIL,
    "name":"Name unit test",
};
    describe('Before Login Page', () => {
        const loginPayload = { email: process.env.LOGIN_EMAIL, password: process.env.LOGIN_PASS };
        const wrongLoginPayload = { email: process.env.LOGIN_EMAIL, password: process.env.LOGIN_PASS+'90' };
        it('Login Api Success', (done) => {
            chai.request(app).post('/login').send(loginPayload).end((err, response) => {
                response.should.have.status(200);
                response.body.should.be.a('object');
                response.body.should.have.property('message');
                //response.body.message.to.eq('Login successfully')
                response.body.should.have.property('token');
                token = response.body.token;
                done();
            });
        });
        it('Login Api Failed', (done) => {
            apiRequest.FailureCallApiPost('/login', wrongLoginPayload);
            done();
        });
       /*  it('register Api Success', (done) => {
            apiRequest.successCallApiPost('/register', registerPayload);
            done();
        }); */
        it('register Api Failed', (done) => {
            apiRequest.FailureCallApiPost('/register', wrongRegisterPayload);
            done();
        });

    });
    describe('Testing User list api', () => {
        it('List Api Success', (done) => {
            apiRequest.successCallApiGet('/users/', token);
            done();
        });
        it('List Api Failed', (done) => {
            apiRequest.failureCallApiGet('/users/', token+'8');
            done();
        });
        it('List Api with ID Success', (done) => {
            apiRequest.successCallApiGet('/users/1', token);
            done();
        });
        it('List Api with ID Failed', (done) => {
            apiRequest.failureCallApiGet('/users/testtest', token);
            done();
        });
        /* it('User Create Success', (done) => {
            apiRequest.successCallApiPost('/users/create', registerPayload, token);
            done();
        }); */
        it('User Create Failed', (done) => {
            apiRequest.FailureCallApiPost('/users/create', wrongRegisterPayload, token);
            done();
        }); 
        /* it('User Update Success', (done) => {
            apiRequest.successCallApiPost('/users/9', updatePayload, token);
            done();
        });*/
        it('User Update Failed', (done) => {
            apiRequest.FailureCallApiPost('/users/909', updatePayload, token);
            done();
        });
        /* it('User Change Password Success', (done) => {
            apiRequest.successCallApiPost('/users/changepassword/19', {
                "oldpassword": process.env.SAMPLE_PASS,
                "newpassword":process.env.DEFAULT_PASS,
                "confirmpassword":process.env.DEFAULT_PASS
            }, token);
            done();
        }); */
        it('User Change Password Failed', (done) => {
            apiRequest.FailureCallApiPost('/users/changepassword/73737', {
                "oldpassword":'',
                "newpassword":process.env.DEFAULT_PASS,
                "confirmpassword":process.env.DEFAULT_PASS
            }, token);
            done();
        });
    });
});