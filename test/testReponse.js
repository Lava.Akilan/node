const chai =  require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');

chai.use(chaiHttp);
chai.should();

async function successCallApiPost(rotueUrl, params, tokenvalues = null){
    return chai.request(app).post(rotueUrl).set({Authorization: 'Bearer '+tokenvalues}).send(params).end((err, response) => {
        response.should.have.status(200);
        response.body.should.be.a('object');
        response.body.should.have.property('message');
    });
}
async function FailureCallApiPost(rotueUrl, params, tokenvalues = null){
    return chai.request(app).post(rotueUrl).set({Authorization: 'Bearer '+tokenvalues}).send(params).end((err, response) => {
        response.status.should.be.oneOf([422, 404, 500, 403, 401]);
        response.body.should.be.a('object');
        response.body.should.satisfy((body) => {
            return body.hasOwnProperty('error') || body.hasOwnProperty('message');
        });
    });
}

async function successCallApiGet(rotueUrl, tokenvalues = null){
    return chai.request(app).get(rotueUrl).set({Authorization: 'Bearer '+tokenvalues}).end((err, response) => {
        response.should.have.status(200);
        response.body.should.be.a('object');
        response.body.should.have.property('data');
    });
}
async function failureCallApiGet(rotueUrl, tokenvalues = null){
    return chai.request(app).get(rotueUrl).set({Authorization: 'Bearer '+tokenvalues}).end((err, response) => {
        response.status.should.be.oneOf([422, 404, 500, 403, 401]);
        response.body.should.be.a('object');
        response.body.should.satisfy((body) => {
            return body.hasOwnProperty('error') || body.hasOwnProperty('message');
        });
    });
}

module.exports = {
    successCallApiPost,
    FailureCallApiPost,
    successCallApiGet,
    failureCallApiGet
}