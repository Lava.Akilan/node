const models = require ('../models');
const User = models.User
const { hashSync, genSaltSync, compareSync } = require("bcrypt");
const jsonwebtoken = require('jsonwebtoken');

const getUserEmail =  async (email) => {
    try {
        const userData = await User.findOne({
            where: {
                email : email
            }
        });
        return userData;
    } catch (error) {
        return error
    }
}

const register = async (req, res) => {
    try {
        let password = req.body.password;
        const salt = genSaltSync(10);
        password = hashSync(password, salt);
        req.body.password = password;
        const data = req.body;
        data.roleType = 2;
        await User.create(data);
        res.status(200).json({
            status: 200,
            message:"Registration successfull"
        });
    } catch (error) {
        res.status(500).json({ error: 'Internal Server Error' });
    }
}

const login = async (req, res) => {
    try {
        let password = req.body.password;
        let user = await getUserEmail(req.body.email);
        const isValidPassword = compareSync(password, user.password);
        if (!isValidPassword){
            return res.status(422).json({
                status:422,
                message:'In correct password'
            })
        }
        const jsontoken = jsonwebtoken.sign({user: user}, process.env.SECRET_KEY, { expiresIn: '60m'} ); 
        res.cookie('token', jsontoken, { httpOnly: true, secure: true, SameSite: 'strict' , expires: new Date(Number(new Date()) + 30*60*1000) }); //we add secure: true, when using https.

        res.status(200).header("Bearer", jsontoken).json({
            status: 200,
            message: 'Login successfully',
            token: jsontoken
        });

    } catch (error) {
        res.status(500).json({
            status:500,
            error: 'Internal Server Error'
        })
    }
}
module.exports = {
    getUserEmail,
    register,
    login
}
