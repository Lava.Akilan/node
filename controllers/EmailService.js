const nodemailer = require('nodemailer');
const  models = require ('../models');
const emailTempmodels = models.Mastermailtemplate


const sendAnMail = async (req, res) => {
    try {
        const transporter = nodemailer.createTransport({
            secure: true,
            requireTLS: true,
            port: 465,
            secured: true,
            service: 'gmail',
            auth: {
              user: process.env.EMAIL_ID,
              pass: process.env.EMAIL_PASS
            }
        });
        let toemail = req.body.toemail;
        let sub = req.body.subject;
        let text = req.body.content;
        const mailOptions = {
            from: process.env.EMAIL_ID,
            to: toemail,
            subject: sub,
            text: text
        };
        
        transporter.sendMail(mailOptions, function(error, info){
            if (error) {
                return res.status(500).json({ 
                    status: 500,
                    error: error 
                });
            } 
            res.status(200).json({ 
                status: 200,
                message: info.response 
            }); 
        });
    } catch (error) {
        res.status(500).json({ 
            status: 500,
            error: 'Internal Server Error' 
        });
    }
}
const emailTemplate = async (req, res) => {
    try {
        const emailkey = req.body.mailkey;
        let templateCheck = await emailTempmodels.findOne({
            where:{mailkey:emailkey}
        })
        if(!templateCheck) {
            return res.status(404).json({ 
                status: 404,
                error: 'Template not exists' 
            });
        }
        let temp = templateCheck.template; //main string
        let repStr = temp.match(/(#[a-zA-Z0-9_ ]*?#)/g);  // Corrected regular expression
        if (repStr) {
            repStr.forEach(match => {
              temp = temp.replace(match, "W3Schools");
            });
        }

        res.status(200).json({
            status:200,
            message:'Success',
            data:temp
        });
    } catch(error) {
        res.status(500).json({ 
            status: 500,
            error: 'Internal Server Error' 
        });
    }
}

module.exports = {
    sendAnMail,
    emailTemplate
}
