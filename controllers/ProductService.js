const models = require ('../models');
const Product = models.Product;
const Topic = models.Topic;

const listAll = async (req, res) => {
    try {
        const result = await Product.findAndCountAll({
            include: [{
                model: Topic,
                as: 'topics',
                through: { attributes: ['product'] }
            }]
            });

        if (!result) {
            return res.status(404).json({ 
                status: 404,
                error: 'Data not found' 
            });
        }
        res.status(200).json({
            status: 200,
            data:result,
        });
    } catch (error) {
        res.status(500).json({ 
            status: 500,
            error: 'Internal Server Error' 
        });
    }
}
const listByid = async (req, res) => {
    try {
        let id = req.params.id;
        const result = await Product.findOne({
            include: [{
                model: Topic,
                as: 'topics',
                through: { attributes: ['product'] }
            }],
            where: {
                id:id
            }
        });

        if (!result) {
            return res.status(404).json({ 
                status: 404,
                error: 'Data not found' 
            });
        }
        res.status(200).json({
            status: 200,
            data:result,
        });
    } catch (error) {
        res.status(500).json({ 
            status: 500,
            error: 'Internal Server Error' 
        });
    }
}
module.exports = {
    listAll,
    listByid
}