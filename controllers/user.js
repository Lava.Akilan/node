const Sequelize = require('sequelize');
const op = Sequelize.Op;

const models = require ('../models');
const User = models.User
const xlsx = require('xlsx');
const { hashSync, genSaltSync, compareSync } = require("bcrypt");
const userHcpRep = models.UserHcpRep;


const create = async (req, res) => {
    try {
        let password = req.body.password;
        const salt = genSaltSync(10);
        password = hashSync(password, salt);
        req.body.password = password;
        const data = req.body;
        await User.create(data);
        res.status(201).json( {
            status: 201,
            message: "user created successfully"
        });
    } catch (error) {
        res.status(500).json({ 
            status: 500,
            error: 'Internal Server Error' 
        });
    }
}
const bulkinsert = async (req, res) => {
    try {
        let filename = req.file.path;
        const workbook = xlsx.readFile(filename);

        // Assuming the data is in the first sheet of the workbook
        const sheetName = workbook.SheetNames[0];
        const worksheet = workbook.Sheets[sheetName];

        // Parse XLSX data
        const jsonData = xlsx.utils.sheet_to_json(worksheet);

        // Use Sequelize to bulk create records
        User.bulkCreate(jsonData)
        .then(() => {
            res.status(200).json({
                status: 200,
                error: 'Data imported successfully'
            });
        })
        .catch(error => {
            res.status(500).json({
                status: 500,
                error: 'Error importing data'
            });
        });
    } catch (error) {
        res.status(500).json({ 
            status: 500,
            error: 'Internal Server Error' 
        });
    }
}
const update = async (req, res) => {
    try {
        const id = req.params.id;
        const data = [];
        data.email = req.body.email;
        data.name =req.body.name;
        
        const userData = await User.findByPk(id);
        if(!userData){
            return res.status(404).json({
                status: 404,
                message: "user not found"
            });
        }
        await User.update(data, {where:{id:id}});
        res.status(200).json({
            status: 200,
            message: "Updated successfully"
        });
    } catch (error) {
        res.status(500).json({ 
            status: 500,
            error: 'Internal Server Error' 
        });
    }
}
const updatePassword = async(req, res) => {
    try {
        const id = req.params.id;
        const userData = await User.findByPk(id);
        if(!userData){
            return res.status(404).json({
                status: 404,
                message: "user not found"
            });
        }
        const isValidPassword = compareSync(req.body.oldpassword, userData.password);
        if (!isValidPassword){
            return res.status(422).json({
                status:422,
                message:'Old password does not match'
            });
            
        } 
        const salt = genSaltSync(10);
        let password = hashSync(req.body.newpassword, salt)
        const data = [];
        data.password = password;
        await User.update(data, {where:{id:id}});
        res.status(200).json({
            status: 200,
            message: "Password changed successfully"
        });
    } catch(error) {
        res.status(500).json({
            status: 500,
            error: 'Internal Server Error'
        })
    }
}
const deleteUser = async (req, res) => {
    try {
        const id = req.params.id;
        const userData = await User.findByPk(id);
        if(!userData) {
            return res.status(404).json({
                status:404,
                messsage: "user not found"
            });
        }
        userData.destroy();
        res.status(200).json({
            status:200,
            messsage: "Deleted successful"
        });
        
    } catch (error) {
        res.status(500).json({ 
            status: 500,
            error: 'Internal Server Error' 
        });
    }
}
const getAll = async (req, res) => {
    try {
        let whereCond={};
        if(req.params.role == 2){
            whereCond = {id: req.params.userid};
            
        } 
        
        const userData = await User.findAndCountAll({where: whereCond});
        const connt = await User.count({
            where:{
                createdAt:  {
                    [op.gt]: '2024-03-01',
                    [op.lt]: '2024-03-06'
                },
            },
            
        });
        if (userData.count == 0) {
            return res.status(404).json({ 
                status: 404,
                error: 'Data not found' 
            });
        }
        res.status(200).json({
            status: 200,
            countbydate: connt,
            data:userData,
            
        });
    } catch (error) {
        res.status(500).json({ 
            status: 500,
            error: error
        });
    }
}
const getById = async (req, res) => {
    const id = req.params.id;
    try {
        const userData = await User.findByPk(id);
        if (!userData) {
            return res.status(404).json({ 
                status:404,
                error: 'Data not found' 
            });
        } 
        res.status(200).json({
            status:200,
            data: userData
        });
    } catch (error) {
        res.status(500).json({ 
            status:500,
            error: 'Internal Server Error' 
        });
    }
}
const exportUser = async (req, res) => {
    try {
        const hcp = await User.findAll({
            raw: true
        });
        const headings = [
            ['Id', 'Name', 'Email', 'Password', 'Role', 'created Date', 'updated Date']
        ]; 

        if(!hcp) {
            return res.status(404).send({
                status: 404,
                message: "No data found",
            }); 
        }
        let fields = ['id', 'name', 'email', 'password', 'role', 'createdAt', 'updatedAt'];
        const wb = xlsx.utils.book_new();
        const ws = xlsx.utils.json_to_sheet(hcp, { 
            origin: 'A2', 
            skipHeader: true 
        }, fields);
        xlsx.utils.sheet_add_aoa(ws, headings); 
        xlsx.utils.book_append_sheet(wb, ws, 'data');

        const buffer = xlsx.write(wb, { bookType: 'csv', type: 'buffer' }); 
        res.attachment('users.csv');
        res.send(buffer);
    } catch (err) {
        res.status(500).send({
            status: 500,
            message: "Something went wrong",
        });
    }
}
const getUserByEmail =  async (email, id = null) => {
    try {
        if(id != null) {
            return await User.findOne({
                where: {
                    email : email,
                    id: {
                        [op.not]: id
                    }
                }
            });
        }
       return  await User.findOne({
            where: {
                email : email
            }
        });
    } catch (error) {
        return error
    }
}
const advancesearchList = async (req, res) => {
    try {
        const dataFilters = req.body;
        let page  = parseInt(req.query.page);
        let limitpage = parseInt(req.query.limit);
        let offset  = 0 + (page - 1) * limitpage;
        let filter = {};
        for (let dat in dataFilters) {
            filter[dat] = {
                    [op.like]: '%' + dataFilters[dat] + '%'
            };     
        }
        const userDatalist = await userHcpRep.findAndCountAll({
            where: filter,
            offset: offset,
            limit: limitpage,
        });
        if (!userDatalist) {
            return res.status(404).json({ 
                status: 404,
                error: 'Data not found' 
            });
        }
        res.status(200).json({
            status: 200,
            data:userDatalist
        });
    } catch (error) {
        res.status(500).json({ 
            status: 500,
            error: error 
        });
    }
}
const searchList = async (req, res) => {
    try {
        const search = req.body.search;
        const dataFilters = ['firstName', 'lastName', 'fullName', 'email', 'telephone', 'speciality', 'degree'];
        let page  = parseInt(req.query.page);
        let limitpage = parseInt(req.query.limit);
        let offset  = 0 + (page - 1) * limitpage;
        let filter = {};
        for (let dat of dataFilters) {
            filter[dat] = {
                    [op.like]: '%' + search + '%'
            };     
        }
        const userDatalist = await userHcpRep.findAndCountAll({
            attributes: [
                'id',
                'firstName',
                'lastName',
                'email'
                /* include: [
                    [Sequelize.fn('COUNT', Sequelize.col('id')), 'n_hats']
                ] */
            ],
            where: {[op.or]: filter},
            order:[
                ['id', 'DESC'],
                ['firstName', 'ASC']
            ],
            offset: offset,
            limit: limitpage, 
        });
        if (!userDatalist) {
            return res.status(404).json({ 
                status: 404,
                error: 'Data not found' 
            });
        }
        res.status(200).json({
            status: 200,
            data:userDatalist
        });

    } catch(error) {
        res.status(500).json({ 
            status: 500,
            error: error 
        });
    }
}
module.exports = {
    create,
    update,
    deleteUser,
    getAll,
    getById,
    bulkinsert,
    exportUser,
    getUserByEmail,
    advancesearchList,
    searchList,
    updatePassword
}
