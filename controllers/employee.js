const models = require ('../models');
const Employee = models.Employee
const register = async (req, res) => {
    const data = req.body;
    await Employee.create(data);
   
    res.json("added successful");
}
const selectAllUsers = async (req, res) => {
    let result = await Employee.findAll();
   
    res.json(result);
}
module.exports = {
register,
selectAllUsers
}
