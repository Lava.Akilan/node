const config = {
  development: {
    database: {
      host: process.env.DB_HOST,
      username: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      dialect: "mysql", // Change this based on your database type
    },
    server: {
      port: 3306,
    },
  },
  production: {
    // Add production configuration here
  },
};

module.exports = config;
