const express = require('express');
const router = express.Router();
const prodCont = require('../controllers/ProductService');
const useMid = require('../middleware/userMiddleware');

router.get('/',  prodCont.listAll);
router.get('/:id', useMid.validationUrl,  prodCont.listByid);

module.exports = router;
