const express = require('express');
const router = express.Router();
const multer = require('multer');
const upload = multer({ dest: 'uploads/',
    limits: {
        fileSize: 8000000 // Compliant: 8MB
    }
});
const userController = require('../controllers/user');
const useMid = require('../middleware/userMiddleware');
const verfiyCookieToken = require('../middleware/verifyToken');

/* GET users listing. */
router.get('/', verfiyCookieToken.verifyUserToken, userController.getAll);
router.get('/:id', verfiyCookieToken.verifyTokenandAllowadmin, useMid.validationUrl, userController.getById);
router.post('/create', verfiyCookieToken.verifyTokenandAllowadmin, useMid.checkDuplication, userController.create);
router.put('/:id', verfiyCookieToken.verifyUserToken, useMid.validationUrl, useMid.checkDuplicationUpdate, userController.update);
router.delete('/:id', verfiyCookieToken.verifyTokenandAllowadmin, useMid.validationUrl, userController.deleteUser);
router.post('/changepassword/:id', verfiyCookieToken.verifyUserToken, useMid.validationUrl, useMid.checkPassword, userController.updatePassword);

//download and import
router.get('/download/1', verfiyCookieToken.verifyTokenandAllowadmin, userController.exportUser);
router.post('/upload',  upload.single('file'), useMid.fileValidate, userController.bulkinsert);

module.exports = router;
