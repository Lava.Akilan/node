const express = require('express');
const router = express.Router();
const authController = require('../controllers/authController');
const authMiddleware = require('../middleware/authMiddleware');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'My title' });
});
router.post('/register', authMiddleware.registerCheck, authController.register);
router.post('/login', authMiddleware.loginVerfiy, authController.login);

module.exports = router;
