const express = require('express');
const router = express.Router();
const emailSer = require('../controllers/EmailService');
const emailMiddleware = require('../middleware/emailMiddleware');

router.post('/',  emailMiddleware.payLoadValidate, emailSer.sendAnMail);
router.post('/template/', emailMiddleware.validationString, emailSer.emailTemplate);


module.exports = router;
