const express = require('express');
const router = express.Router();
const empController = require('../controllers/employee.controller')

const multer = require('multer');

const upload = multer({ dest: 'uploads/',
    limits: {
        fileSize: 8000000 // Compliant: 8MB
    }
});
const xlsx = require('xlsx');


router.post('/upload',  upload.single('file'), (req, res) => {

    // File handling logic will be implemented here
    const workbook = xlsx.readFile(req.file.path);

    const worksheet = workbook.Sheets[workbook.SheetNames[0]];

    const data = xlsx.utils.sheet_to_json(worksheet, { header: 1 });
    const valuesdata = data.map(row => [row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12], row[13], row[14]]); // Replace with your column mappings

    const sql = 'INSERT INTO userhcprep (firstName, lastName, fullName, email, telephone, degree, speciality, address, city, state, photo, photo_path, npi, address2, state_license_no) VALUES ?';
    db.query(sql, [valuesdata],(err, results) => {
         if (err) {
             res.json(err);
         }
         res.send('Data uploaded successfully');
     });
});

router.get('/download', function(req, res) {
    const hcp = empController.selectAllUsers(); 

    const headings = [
        ['Id', 'firstName', 'lastName', 'fullName', 'email', 'telephone', 'degree', 'speciality', 'address', 'city', 'state', 'photo', 'photo_path', 'npi', 'address2', 'state_license_no']
    ]; 

    const wb = xlsx.utils.book_new();

    hcp.then( function(value){
        const ws = xlsx.utils.json_to_sheet(value, { 
            origin: 'A2', 
            skipHeader: true 
        });
        xlsx.utils.sheet_add_aoa(ws, headings); 
        xlsx.utils.book_append_sheet(wb, ws, 'data');
    
        const buffer = xlsx.write(wb, { bookType: 'csv', type: 'buffer' }); 
        res.attachment('userhcpdata.csv');
        res.send(buffer);  
    }).catch( function(error){console.log( error )});
});

/* GET users listing. */
router.get('/', function(req, res, next) {
    let resultdata = employee.selectAll();
    resultdata.then( function(value){console.log( value ); res.json( value)})
    .catch( function(error){console.log( error )});
});

router.get('/:id', function(req, res, next) {
    let id = req.params.id;
    db.query('select * from employeedetails where id ='+id, (err, results) => {
        if(err) {
            res.json(err);
        }
        res.json(results)
    })
});

router.get('/list', function(req, res, next) {
    let resultdata =  db.query('SELECT * FROM employeedetails', (err, results) => {
        if (err) {
            return err;
        }
        return results;
    });
    return resultdata;
});

router.post('/', function(req, res, next) {
    const { name, email, id } = req.body;
    let msg ;
    if(id) {
        msg = 'updated';
        const sql = 'UPDATE  employeedetails SET name=? , email=? WHERE id = ?';
        db.query(sql, [name, email, id],(err, results) => {
            if (err) {
                return err;
            }
            return results;
        });

    } else {
        const sql = 'INSERT INTO employeedetails (name, email, file) VALUES (?, ?, ?)';
        db.query(sql, [name, email, ''],(err, results) => {
            if (err) {
                return err;
            }
            return results;
        });
        msg = 'created';
    }
   
  res.send('Form '+msg+' successfully');
});

router.delete('/:id', function(req, res, next) {

    let id = req.params.id;
    const sql = 'Delete from employeedetails where id ='+id;
        db.query(sql,(err, results) => {
            if (err) {
                return err;
            }
            return results;
        });
        res.send('Form deleted successfully');
  });
  
module.exports = router;
