'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Users', [{
      firstName: 'John',
      lastName: 'Doe',
      fullName: 'John Doe',
      email: 'example@example.com',
      telephone:'9090-909-909',
      speciality:'ABDOMINAL RADIOLOGY',
      degree:'BA',
      address:'Address come here',
      city:'New york',
      state:'NA',
      address2:'test address2',
      createdAt: new Date(),
      updatedAt: new Date()
    }]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * 
     */
     await queryInterface.bulkDelete('Userhcprep', null, {});
  }
};
