const Joi = require('joi') 
const userschemas = data  => {
  const schema = Joi.object({
      name: Joi.string()
          .min(6)
          .required(),
      email: Joi.string()
          .min(6)
          .required()
          .email(),
      password: Joi.string()
          .min(8)
          .required()
  });
  return schema.validate(data);
};
const userschemasUpdate = data => {
    const schema = Joi.object({
        name: Joi.string()
            .min(6)
            .optional(),
        email: Joi.string()
            .min(6)
            .optional()
            .email()
        //id: Joi.number().required()
    });
    return schema.validate(data);
}
const idValidate = (id) => {
    const schema = Joi.number().required();
    return schema.validate(id);
}

const mymethod = (value) => {
    let errorMsg = null;
    // Throw an error (will be replaced with 'any.custom' error)
    if (!value) {
        errorMsg = 'File is required';
    }
    if (!['application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'].includes(value.mimetype)) {
        // If file MIME type is not supported, return an error
       errorMsg= 'File type should be excel document';
    }

    const fileExtension = value.originalname.split('.').pop().toLowerCase();
    if (!['xlsx'].includes(fileExtension)) {
        errorMsg = 'File type should be excel document' ;
    }

    // Check original filename for extension
    const fileNameParts = value.originalname.split('.');
    if (fileNameParts.length > 2) {
        errorMsg =  'Please upload a file with valid extension';
    }
    // Return the value unchanged
    return errorMsg;
};


const fileValidate = (file) => {
    const schema = Joi.any().custom(mymethod, 'custom validation');
    return schema.validate(file);
}
const passwordMatch = (data) => {
    const schema = Joi.object({
        oldpassword: Joi.string()
            .min(8)
            .required(),
        newpassword: Joi.string()
            .min(8)
            .required(),
        confirmpassword: Joi.ref('newpassword')
    });
    return schema.validate(data);
}

module.exports = {
  userschemas,
  userschemasUpdate,
  idValidate,
  fileValidate,
  passwordMatch
};
