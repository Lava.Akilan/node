const Joi = require('joi') 
const emailValidation = data  => {
  const schema = Joi.object({
    subject: Joi.string()
          .required(),
    toemail: Joi.string()
          .min(6)
          .required()
          .email(),
    content: Joi.string()
          .required()
  });
  return schema.validate(data);
};
const stringValidate = mailkey => {
    const schema = Joi.string().required();
    return schema.validate(mailkey);
}
module.exports = {
    emailValidation,
    stringValidate
}